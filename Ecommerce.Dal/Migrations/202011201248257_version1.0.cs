namespace Ecommerce.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adres",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        baslik = c.String(),
                        icerik = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Blog",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        KategoriID = c.Int(nullable: false),
                        KullaniciID = c.Int(nullable: false),
                        baslik = c.String(),
                        icerik = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kategori", t => t.KategoriID, cascadeDelete: true)
                .ForeignKey("dbo.Kullanici", t => t.KullaniciID, cascadeDelete: true)
                .Index(t => t.KategoriID)
                .Index(t => t.KullaniciID);
            
            CreateTable(
                "dbo.BlogYorum",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        blogID = c.Int(nullable: false),
                        baslik = c.String(),
                        icerik = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Blog", t => t.blogID, cascadeDelete: true)
                .Index(t => t.blogID);
            
            CreateTable(
                "dbo.Kategori",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ad = c.String(nullable: false),
                        UstKategoriID = c.Int(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kategori", t => t.UstKategoriID)
                .Index(t => t.UstKategoriID);
            
            CreateTable(
                "dbo.Kullanici",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ad = c.String(nullable: false, maxLength: 25),
                        soyad = c.String(nullable: false),
                        telefon = c.Int(nullable: false),
                        email = c.String(nullable: false),
                        sifre = c.String(nullable: false),
                        sifreOnay = c.String(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Rol",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ad = c.String(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Fotograf",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        url = c.String(nullable: false),
                        urunID = c.Int(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Urun", t => t.urunID, cascadeDelete: true)
                .Index(t => t.urunID);
            
            CreateTable(
                "dbo.Urun",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ad = c.String(nullable: false),
                        Fiyat = c.Int(nullable: false),
                        KategoriID = c.Int(nullable: false),
                        Marka = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        Stok = c.Int(nullable: false),
                        Aciklama = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kategori", t => t.KategoriID, cascadeDelete: true)
                .Index(t => t.KategoriID);
            
            CreateTable(
                "dbo.UrunYorum",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        urunID = c.Int(nullable: false),
                        baslik = c.String(),
                        icerik = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Urun", t => t.urunID, cascadeDelete: true)
                .Index(t => t.urunID);
            
            CreateTable(
                "dbo.İletisim",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ad = c.String(nullable: false, maxLength: 25),
                        soyad = c.String(nullable: false),
                        mesaj = c.String(),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Sepet",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        kullaniciID = c.Int(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kullanici", t => t.kullaniciID, cascadeDelete: true)
                .Index(t => t.kullaniciID);
            
            CreateTable(
                "dbo.SepetUrun",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        sepetID = c.Int(nullable: false),
                        urunID = c.Int(nullable: false),
                        Miktar = c.Int(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Sepet", t => t.sepetID, cascadeDelete: true)
                .ForeignKey("dbo.Urun", t => t.urunID, cascadeDelete: true)
                .Index(t => t.sepetID)
                .Index(t => t.urunID);
            
            CreateTable(
                "dbo.Siparis",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        adresID = c.Int(nullable: false),
                        sepetID = c.Int(nullable: false),
                        TeslimTarihi = c.DateTime(nullable: false),
                        KayitTarihi = c.DateTime(nullable: false),
                        isdelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Adres", t => t.adresID, cascadeDelete: true)
                .ForeignKey("dbo.Sepet", t => t.sepetID, cascadeDelete: true)
                .Index(t => t.adresID)
                .Index(t => t.sepetID);
            
            CreateTable(
                "dbo.RolKullanici",
                c => new
                    {
                        Rol_id = c.Int(nullable: false),
                        Kullanici_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Rol_id, t.Kullanici_id })
                .ForeignKey("dbo.Rol", t => t.Rol_id, cascadeDelete: true)
                .ForeignKey("dbo.Kullanici", t => t.Kullanici_id, cascadeDelete: true)
                .Index(t => t.Rol_id)
                .Index(t => t.Kullanici_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Siparis", "sepetID", "dbo.Sepet");
            DropForeignKey("dbo.Siparis", "adresID", "dbo.Adres");
            DropForeignKey("dbo.SepetUrun", "urunID", "dbo.Urun");
            DropForeignKey("dbo.SepetUrun", "sepetID", "dbo.Sepet");
            DropForeignKey("dbo.Sepet", "kullaniciID", "dbo.Kullanici");
            DropForeignKey("dbo.Fotograf", "urunID", "dbo.Urun");
            DropForeignKey("dbo.UrunYorum", "urunID", "dbo.Urun");
            DropForeignKey("dbo.Urun", "KategoriID", "dbo.Kategori");
            DropForeignKey("dbo.Blog", "KullaniciID", "dbo.Kullanici");
            DropForeignKey("dbo.RolKullanici", "Kullanici_id", "dbo.Kullanici");
            DropForeignKey("dbo.RolKullanici", "Rol_id", "dbo.Rol");
            DropForeignKey("dbo.Blog", "KategoriID", "dbo.Kategori");
            DropForeignKey("dbo.Kategori", "UstKategoriID", "dbo.Kategori");
            DropForeignKey("dbo.BlogYorum", "blogID", "dbo.Blog");
            DropIndex("dbo.RolKullanici", new[] { "Kullanici_id" });
            DropIndex("dbo.RolKullanici", new[] { "Rol_id" });
            DropIndex("dbo.Siparis", new[] { "sepetID" });
            DropIndex("dbo.Siparis", new[] { "adresID" });
            DropIndex("dbo.SepetUrun", new[] { "urunID" });
            DropIndex("dbo.SepetUrun", new[] { "sepetID" });
            DropIndex("dbo.Sepet", new[] { "kullaniciID" });
            DropIndex("dbo.UrunYorum", new[] { "urunID" });
            DropIndex("dbo.Urun", new[] { "KategoriID" });
            DropIndex("dbo.Fotograf", new[] { "urunID" });
            DropIndex("dbo.Kategori", new[] { "UstKategoriID" });
            DropIndex("dbo.BlogYorum", new[] { "blogID" });
            DropIndex("dbo.Blog", new[] { "KullaniciID" });
            DropIndex("dbo.Blog", new[] { "KategoriID" });
            DropTable("dbo.RolKullanici");
            DropTable("dbo.Siparis");
            DropTable("dbo.SepetUrun");
            DropTable("dbo.Sepet");
            DropTable("dbo.İletisim");
            DropTable("dbo.UrunYorum");
            DropTable("dbo.Urun");
            DropTable("dbo.Fotograf");
            DropTable("dbo.Rol");
            DropTable("dbo.Kullanici");
            DropTable("dbo.Kategori");
            DropTable("dbo.BlogYorum");
            DropTable("dbo.Blog");
            DropTable("dbo.Adres");
        }
    }
}
