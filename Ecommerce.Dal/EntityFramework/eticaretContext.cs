namespace Ecommerce.Dal.EntityFramework
{
    using Ecommerce.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class eticaretContext : DbContext
    {
        // Your context has been configured to use a 'eticaretContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Ecommerce.Dal.EntityFramework.eticaretContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'eticaretContext' 
        // connection string in the application configuration file.
        public eticaretContext()
            : base("name=eticaretContext")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
             Database.SetInitializer<eticaretContext>(new CreateDatabaseIfNotExists<eticaretContext>());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Kullanici> Kullanici { get; set; }
        public virtual DbSet<Adres> Adres { get; set; }
        public virtual DbSet<Blog> Blog { get; set; }
        public virtual DbSet<BlogYorum> BlogYorum { get; set; }
        public virtual DbSet<Fotograf> Fotograf { get; set; }
        public virtual DbSet<�letisim> Iletisim { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<Kategori> Kategori { get; set; }
        public virtual DbSet<Sepet> Sepet { get; set; }
        public virtual DbSet<SepetUrun> SepetUrun { get; set; }
        public virtual DbSet<Siparis> Siparis { get; set; }
        public virtual DbSet<Urun> Urun { get; set; }
        public virtual DbSet<UrunYorum> UrunYorum { get; set; }
    }


}