﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class RolRepository:BaseRepository<Rol,ETicaretContext>
    public class RolRepository : BaseRepository<Rol>
    {
        //TODO:GET ROLS BY USERID
        public List<Rol> GetByUserId(int id)
        {
            //using (context = new ETicaretContext())
            {
                return context.Set<Rol>().Where(x => x.Kullanicis.Contains(new Kullanici { id = id })).ToList();
            }
        }
    }
}
