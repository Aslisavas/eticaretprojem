﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class BlogYorumRepository:BaseRepository<BlogYorum,ETicaretContext>
    public class BlogYorumRepository : BaseRepository<BlogYorum>
    {
        //TODO : GET BY BLOGID
        public List<BlogYorum>GetByBlogId(int id)
        {
            //using (context=new ETicaretContext())
            {
                return context.Set<BlogYorum>().Where(x => x.blogID == id).ToList();
            }
        }
    }
}
