﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class BlogRepository:BaseRepository<Blog,ETicaretContext>

    public class BlogRepository : BaseRepository<Blog >
    {
        //TODO : get by kategoriid
        public List<Blog> GetByCategoryId(int categoryId)
        {
            //using (context = new ETicaretContext())
            {
                return context.Blog.Where(x => x.KategoriID == categoryId).ToList();
            }
        }
    }
}
