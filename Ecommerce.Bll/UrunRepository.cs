﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class UrunRepository : BaseRepository<Urun, ETicaretContext>
    public class UrunRepository:BaseRepository<Urun>
    {
        //TODO : KATEGORİYE AİT ÜRÜN
        public List<Urun>GetByCategoryId(int categoryId)
        {
            //using (context = new ETicaretContext())
            {
                return context.Urun.Where(x => x.KategoriID == categoryId).ToList();
            }
        }
    }
}
