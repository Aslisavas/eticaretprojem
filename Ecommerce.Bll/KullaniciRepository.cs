﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class KullaniciRepository:BaseRepository<Kullanici,ETicaretContext>
    public class KullaniciRepository : BaseRepository<Kullanici>
    {
        public Kullanici Login (string email,string sifre)
        {
           // using (context=new ETicaretContext())
            {
                return context.Set<Kullanici>().Where(x => x.email == email && x.sifre == sifre).FirstOrDefault();
            }
        }
        public bool AddRole(int userid,int roleid) {
            SqlParameter[] parameters = { new SqlParameter("userid", userid), new SqlParameter("roleid", roleid) };
            int result=context.Database.ExecuteSqlCommand("insert into RolKullanici(Rol_id,Kullanici_id)values(@roleid,@userid)", parameters);
            return result > 0 ? true : false;
        }
       
    }
}
