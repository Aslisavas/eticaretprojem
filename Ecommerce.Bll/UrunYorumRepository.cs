﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
    //public class UrunYorumRepository : BaseRepository<UrunYorum, ETicaretContext>
   public class UrunYorumRepository:BaseRepository<UrunYorum>
    {
       public List<UrunYorum>GetByProductId(int id)
        {
            //using (context = new ETicaretContext())
            {
                return context.Set<UrunYorum>().Where(x => x.urunID == id).ToList();
            }
        }
    }
}
