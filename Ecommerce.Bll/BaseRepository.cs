﻿
using Ecommerce.Dal.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
   public class BaseRepository<T> where T : class,new() 
    {
        protected readonly eticaretContext context = new eticaretContext();
        // protected Context context; 
        public bool Add(T data)
        {
            try
            {
               // using (context=new Context())
                {
                   var T= context.Set<T>().Add(data);
                   int result= context.SaveChanges();
                    return result > 0 ? true : false;
                }

            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                //using (context=new Context())
                {
                   var silinecekdata= context.Set<T>().Find(id);
                    context.Set<T>().Remove(silinecekdata);
                    var result = context.SaveChanges();
                    return result > 0 ? true : false;


                }

            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public List<T>GetAll()
        {
           // using (context=new Context())
            {
               return context.Set<T>().ToList();
            }
           
        }
        public T GetById(int id)
        {
           // using (context =new Context())
            {
                return context.Set<T>().Find(id);

            }
            
        }
        public bool Update(T data)
        {
            //using (context =new Context())
            {
                //aynı olanları güncellemez sadece değişeni günceller 
             context.Entry<T>(data).State = EntityState.Modified;
                 int result=  context.SaveChanges();
                return result > 0 ? true : false;
            }
        }
        public List<T> GetByFilter(Expression<Func<T,bool>> filter)
        {

            //using (context=new Context())
            {
                return context.Set<T>().Where(filter).ToList();
            }
        }
    }
}
