﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Model
{
   //public class FotografRepository:BaseRepository<Fotograf,ETicaretContext>
   public class FotografRepository : BaseRepository<Fotograf>

    {
        //TODO : GET BY PRODUCT
        public List<Fotograf> GetByProductId(int id)
        {
            //using (context = new ETicaretContext())
            {
                return context.Set<Fotograf>().Where(x => x.urunID == id).ToList();
            }
        }
    }
}
