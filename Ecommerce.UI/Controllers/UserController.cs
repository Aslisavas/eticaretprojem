﻿using Ecommerce.Model;
using Ecommerce.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ecommerce.UI.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel Model)
        {
            KullaniciRepository repo = new KullaniciRepository();
            var user = repo.Login(Model.email, Model.sifre);
            if (user != null)
            {
                Session["LoginUser"] = user;
                return RedirectToAction("Profile");
            }
            else
            {
                TempData["mesaj"] = new TempDataDictionary { { "class", "alert-danger" }, { "icerik", "Email veya Şifre Hatalı" } };
                return View();
            }
        }
            public ActionResult Register()
            {
                return View();
            }
            [HttpPost]
            public ActionResult Register(RegisterViewModel model)
            {
                KullaniciRepository repo = new KullaniciRepository();
            RolRepository rolRepository = new RolRepository();
                bool result = repo.Add(new Model.Kullanici { ad = model.ad, soyad = model.soyad, email = model.email, telefon = model.telefon, sifre = model.sifre, sifreOnay = model.sifreOnay, isdelete = false, KayitTarihi = DateTime.Now });
            var userid = repo.GetByFilter(x => x.email == model.email).FirstOrDefault().id;
            var roleid = rolRepository.GetByFilter(x => x.ad == "admin").FirstOrDefault().id;
            repo.AddRole(userid, roleid);
            TempData["mesaj"] = result == true ? new TempDataDictionary { { "class", "alert-success" }, { "icerik", "Kayıt Eklendi" } } : new TempDataDictionary { { "class", "alert-danger" }, { "icerik", "Kayıt Başarısız" } };
                return View();
            }
        public ActionResult Profile()
        {
            return View();
        }
            
        
    }
}