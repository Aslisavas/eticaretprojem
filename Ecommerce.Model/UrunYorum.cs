﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class UrunYorum:Base
    {
        [ForeignKey("Urunu")]
        public int urunID { get; set; }
        public string baslik { get; set; }
        public string icerik { get; set; }

        public virtual Urun Urunu { get; set; }
    }
}