﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class BlogYorum:Base
    {
       [ForeignKey("Blogu")]
        public int blogID { get; set; }
        public string baslik { get; set; }
        public string icerik { get; set; }
        //navi prop
        public virtual Blog Blogu { get; set; }
    }
}