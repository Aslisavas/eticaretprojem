﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Kullanici:Base
    {
       public Kullanici()
        {
            this.Rols = new HashSet<Rol>();
        }
        [Required]
        [StringLength(25,ErrorMessage="Ad en fazla 25 karakter uzunluğunda olmalıdır.",MinimumLength =2)]
        public string ad { get; set; }
        [Required]
        public string soyad{get;set;} 
        
        [DataType(DataType.PhoneNumber)]
        public int telefon { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string sifre { get; set; }
        [Required]
        [Compare("sifre")]
        [DataType(DataType.Password)]
        public string sifreOnay { get; set; }
       
       
       
        //nav prop
        public virtual ICollection<Rol>Rols { get; set; }


    }
}