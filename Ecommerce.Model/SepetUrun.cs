﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class SepetUrun:Base
    {   [Required]
        public int sepetID { get; set; }
        [Required]
        public int urunID { get; set; }
        [Required]
        public int Miktar { get; set; }

        //NAV PROP
        [ForeignKey("sepetID")]
        public virtual Sepet sepeti { get; set; }
        [ForeignKey("urunID")]
        public virtual Urun urunu { get; set; }

    }
}