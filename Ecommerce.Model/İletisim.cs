﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class İletisim:Base
    {
        [Required]
        [StringLength(25, ErrorMessage = "Ad en fazla 25 karakter uzunluğunda olmalıdır.", MinimumLength = 2)]
        public string ad { get; set; }
        [Required]
        public string soyad { get; set; }
        public string mesaj { get; set; }

    }
}