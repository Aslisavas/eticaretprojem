﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Kategori:Base
    {
        [Required]
        public string ad { get; set; }
       
        public int? UstKategoriID { get; set; }
        //nav prop
        [ForeignKey("UstKategoriID")]
        public virtual Kategori ustkategorisi { get; set; }
    }
}