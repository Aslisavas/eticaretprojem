﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Rol:Base
    {
        public Rol()
        {
            this.Kullanicis = new HashSet<Kullanici>();
        }
            

       [Required]
        public string ad { get; set; }
        //nav prop
        public virtual ICollection<Kullanici>Kullanicis { get; set; }
    }
}