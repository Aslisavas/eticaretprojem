﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Fotograf:Base
    {   [Required]
        [DataType(DataType.Url)]
        public string url { get; set; }
        [Required]
        [ForeignKey("Urunu")]
        public int urunID { get; set; }
        //navi prop
        public virtual Urun Urunu { get; set; }
    }
}