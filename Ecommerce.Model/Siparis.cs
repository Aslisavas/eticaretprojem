﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Siparis:Base
    {
        [Required]
        [ForeignKey("adresi")]
        public int adresID { get; set; }
        [Required]
        [ForeignKey("sepeti")]
        public int sepetID { get; set; }
        [Required]
        public DateTime TeslimTarihi { get; set; }
        //navi prop
        public virtual Sepet sepeti { get; set; }
        public virtual Adres adresi { get; set; }


    }
}