﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Blog:Base
    {
        public Blog()
        {
            this.BlogYorumlari = new HashSet<BlogYorum>();
        }
        public int KategoriID { get; set; }
        public int KullaniciID { get; set; }
        public string baslik { get; set; }
        public string icerik { get; set; }
        //NAV PROP
        [ForeignKey("KategoriID")]
        public virtual Kategori kategorisi { get; set; }
        [ForeignKey("KullaniciID")]
        public virtual Kullanici kullanicisi { get; set; }
        public virtual ICollection<BlogYorum>BlogYorumlari { get; set; }

    }
}