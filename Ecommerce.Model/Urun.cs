﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Model
{
    public class Urun : Base
    {
        public Urun()
        {
            this.Yorumlari = new HashSet< UrunYorum > ();
        }
      
        [Required]
        public string ad { get; set; }
        [Required]
        public int Fiyat { get; set; }
        [Required]
        [ForeignKey("kategorisi")]
        public int KategoriID { get; set; }
        [Required]
        public string Marka { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public int Stok { get; set; }
        
        public string  Aciklama { get; set; }

        //navigation properties
        public virtual Kategori kategorisi { get; set; }
        public virtual ICollection<UrunYorum> Yorumlari{ get; set; }
    }
}